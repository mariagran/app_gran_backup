<?php

// MÓDULO DE NOTIFICAÇÃO CADASTRO DE CLIENTE
function set_html_content_type() { return 'text/html'; }

function notificacao_cadastrocliente(){

    // DADOS EMRPESA
    $cliente_nome_fantasia      = $_POST['cliente_nome_fantasia'];
    $cliente_tipo               = $_POST['cliente_tipo'];
    $cliente_razao_social       = $_POST['cliente_razao_social'];
    $cliente_cnpj_cpf           = $_POST['cliente_cnpj_cpf'];
    $cliente_inscricao_estadual = $_POST['cliente_inscricao_estadual'];
    $cliente_inscricao_municipal= $_POST['cliente_inscricao_municipal'];
    $cliente_email_principal    = $_POST['cliente_email_principal'];
    $cliente_telefone_comercial = $_POST['cliente_telefone_comercial'];
    $cliente_telefone_celular   = $_POST['cliente_telefone_celular'];

    // ENDEREÇO
    $cliente_cep                = $_POST['cliente_cep'];
    $cliente_endereco           = $_POST['cliente_endereco'];
    $cliente_numero             = $_POST['cliente_numero'];
    $cliente_complemento        = $_POST['cliente_complemento'];
    $cliente_bairro             = $_POST['cliente_bairro'];
    $cliente_cidade             = $_POST['cliente_cidade'];
    $cliente_estado             = $_POST['cliente_estado'];
   
    // DADOS RESPONSÁVEL
    $cliente_responsavel_nome_completo = $_POST['cliente_responsavel_nome_completo'];
    $cliente_responsavel_celular       = $_POST['cliente_responsavel_celular'];
    $cliente_responsavel_email         = $_POST['cliente_responsavel_email'];

    $html  = '<p>Dados do cliente no corpo de e-mail:</p>';

    // DADOS EMRPESA
    $html .= '<hr />';
    $html .= '<b>Nome Fantasia</b>: '      . $cliente_nome_fantasia             . '<br />';
    $html .= '<b>Tipo cliente</b>: '       . $cliente_tipo                      . '<br />';
    $html .= '<b>Razão Social</b>: '       . $cliente_razao_social              . '<br />';
    $html .= '<b>CPF/CNJ</b>: '            . $cliente_cnpj_cpf                  . '<br />';
    $html .= '<b>Inscrição Estadual</b>: ' . $cliente_inscricao_estadual        . '<br />';
    $html .= '<b>Inscrição Municipal</b>: '. $cliente_inscricao_municipal       . '<br />';
    $html .= '<b>Email Principal</b>: '    . $cliente_email_principal           . '<br />';
    $html .= '<b>Telefone Comercial</b>: ' . $cliente_telefone_comercial        . '<br />';
    $html .= '<b>Telefone Celular</b>: '   . $cliente_telefone_celular          . '<br />';
    
    // ENDEREÇO
    $html .= '<p><b>ENDEREÇO</b></p>';
    $html .= '<b>CEP:</b>: '               . $cliente_cep                       . '<br />';
    $html .= '<b>Endereço:</b>: '          . $cliente_endereco                  . '<br />';
    $html .= '<b>Número</b>: '             . $cliente_numero                    . '<br />';
    $html .= '<b>Complemento</b>: '        . $cliente_complemento               . '<br />';
    $html .= '<b>Bairro</b>: '             . $cliente_bairro                    . '<br />';
    $html .= '<b>Cidade</b>: '             . $cliente_cidade                    . '<br />';
    $html .= '<b>Estado</b>: '             . $cliente_estado                    . '<br />';

    $html .= '<p><b>DADOS RESPONSÁVEL</b></p>';
    // DADOS RESPONSÁVEL
    $html .= '<b>Nome Completo</b>: '      . $cliente_responsavel_nome_completo . '<br />';
    $html .= '<b>Celular</b>: '            . $cliente_responsavel_celular       . '<br />';
    $html .= '<b>Email</b>: '              . $cliente_responsavel_email         . '<br />';

    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
    $envio = wp_mail('hudson@gran.ag', 'Novo cliente na Gran!', $html);
    $envio = wp_mail('willerson@gran.ag ', 'Novo cliente na Gran!', $html);
    $envio = wp_mail('contato@handgran.com ', 'Novo cliente na Gran!', $html);
    remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

    die();
}

add_action('wp_ajax_notificacao_cadastrocliente', 'notificacao_cadastrocliente');
add_action('wp_ajax_nopriv_notificacao_cadastrocliente', 'notificacao_cadastrocliente');




