<?php 

function cadastrar_colaborador_pj(){
	
	/****************************************************
	* DADOS DA EMPRESA MEI                              *
	*****************************************************/
		// PRIMEIRO NOME
		$colaborador_primeiro_nome           = $_POST['colaborador_primeiro_nome'];

		$colaborador_pj_cnh_name           = $_FILES['colaborador_pj_cnh'];		
		$colaborador_pj_cnh_tmp_name       = $_FILES['colaborador_pj_cnh']["tmp_name"];
		$colaborador_pj_nome_fantasia      = $_POST['colaborador_pj_nome_fantasia'];		
		$colaborador_pj_tipo               = $_POST['colaborador_pj_tipo'];		
		$colaborador_pj_razao_social       = $_POST['colaborador_pj_razao_social'];		
		$colaborador_pj_cnpj          	   = $_POST['colaborador_pj_cnpj'];		
		$colaborador_pj_email_principal    = $_POST['colaborador_pj_email_empresarial'];		
		$colaborador_pj_telefone_comercial = $_POST['colaborador_pj_telefone_comercial'];		
		$colaborador_pj_telefone_celular   = $_POST['colaborador_pj_telefone_celular'];		
		

	/****************************************************
	* DADOS RESPONSÁVEL LEGAL                           *
	*****************************************************/
		$responsavel_legal_nome_completo   = $_POST['responsavel_legal_nome_completo'];	
		$responsavel_legal_cpf             = $_POST['responsavel_legal_cpf'];	
		$responsavel_legal_data_nascimento = $_POST['responsavel_legal_data_nascimento'];	
		$responsavel_legal_celular         = $_POST['responsavel_legal_celular'];	
		$responsavel_legal_email           = $_POST['responsavel_legal_email'];

	/****************************************************
	* DADOS ENDEREÇO DA EMPRESA                         *
	*****************************************************/
		$colaborador_pj_cep			= $_POST['colaborador_pj_cep'];	
		$colaborador_pj_endereco	= $_POST['colaborador_pj_endereco'];	
		$colaborador_pj_numero		= $_POST['colaborador_pj_numero'];	
		$colaborador_pj_complemento	= $_POST['colaborador_pj_complemento'];	
		$colaborador_pj_bairro		= $_POST['colaborador_pj_bairro'];	
		$colaborador_pj_cidade		= $_POST['colaborador_pj_cidade'];	
		$colaborador_pj_estado		= $_POST['colaborador_pj_estado'];

	/****************************************************
	* ARRAY PARA CADASTRAR COLABORADOR
	*****************************************************/
		$cadastrar_colaborador_pj  = array(
			'post_title'   => $colaborador_pj_nome_fantasia,
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'	   => 'colaborador',
		);	
		$cliente_ID = wp_insert_post($cadastrar_colaborador_pj);
		wp_set_object_terms($cliente_ID, 33, 'categoriacolaborador');

	/****************************************************
	* INSERT METABOXES  DADOS DO CLIENTE
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_cnh_texto',		 $colaborador_pj_cnh,	     true);
		


		add_post_meta($cliente_ID, 'Gran_colaborador_pj_nome_fantasia',		 $colaborador_pj_nome_fantasia,	     true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_tipo',				 $colaborador_pj_tipo,			     true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_razao_social',		 $colaborador_pj_razao_social,	     true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_cnpj',				 $colaborador_pj_cnpj,			  	 true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_email_empresarial',	 $colaborador_pj_email_principal,    true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_telefone_comercial', $colaborador_pj_telefone_comercial, true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_telefone_celular',	 $colaborador_pj_telefone_celular,   true);

	/****************************************************
	* INSERT METABOXES  DADOS RESPONSÁVEL CLIENTE
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_responsavel_nome_completo',   $responsavel_legal_nome_completo,   true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_responsavel_cpf',             $responsavel_legal_cpf,             true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_responsavel_data_nascimento', $responsavel_legal_data_nascimento, true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_responsavel_celular',         $responsavel_legal_celular,         true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_responsavel_email',           $responsavel_legal_email,           true);

	/****************************************************
	* INSERT METABOXES DADOS ENDREÇO DO CLIENTE
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_cep',	      $colaborador_pj_cep,		   true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_endereco',	  $colaborador_pj_endereco,    true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_numero',	  $colaborador_pj_numero,	   true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_complemento', $colaborador_pj_complemento, true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_bairro',	  $colaborador_pj_bairro,	   true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_cidade',	  $colaborador_pj_cidade,	   true);
		add_post_meta($cliente_ID, 'Gran_colaborador_pj_estado',	  $colaborador_pj_estado,	   true);

	/****************************************************
	* INSERT METABOXES LOGO DO CLIENTE
	*****************************************************/
		// move_uploaded_file($colaborador_pj_cnh_tmp_name,WP_CONTENT_DIR .'/uploads/clientes/'.basename($colaborador_pj_cnh_name));

		// $logo_nome  = WP_CONTENT_DIR .'/uploads/clientes/' . basename($colaborador_pj_cnh_name);
		// $logo_tipo  = wp_check_filetype( basename( $logo_nome ),null);
		// $logo_tipos = array('png','jpg','jpeg','gif');

		// if(in_array($logo_tipo['ext'], $logo_tipos)){

		// 	require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// 	$diretorioUploads    = wp_upload_dir();
		// 	$logo_detalhes       = array(
		// 		'guid'           => $diretorioUploads['url'] . '/' . basename( $logo_nome ), 
		// 		'post_mime_type' => $logo_tipo['type'],
		// 		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $logo_nome ) ),
		// 		'post_content'   => '',
		// 		'post_status'    => 'inherit'
		// 	);

		// 	$logo_carregado_Id 	    = wp_insert_attachment($logo_detalhes, $logo_nome, $cliente_ID);
		// 	$logo_carregado_meta	= wp_generate_attachment_metadata($logo_carregado_Id,$logo_nome);

		// 	wp_update_attachment_metadata($logo_carregado_Id, $logo_carregado_meta);
		// 	add_post_meta($cliente_ID, 'Gran_colaborador_pj_cnh',$logo_carregado_Id,true);
		// }

		wp_insert_term(
		    $colaborador_pj_nome_fantasia,  
		    'categoriacolaborador',
		    array(
		        'description' => $logo_detalhes['post_title'],
		    )
		);

	if($cliente_ID > 0){
		$cadastroRealizado = true;

		$user_id = username_exists( $colaborador_primeiro_nome );
		
		if(!$user_id){
			wp_create_user( $colaborador_primeiro_nome, "123", $colaborador_primeiro_nome . "@gran.ag" );
		}

	/****************************************************
	* TEMPLATE DE NOTIFICAÇÃO
	*****************************************************/
	    $html .= '<table align="center" bgcolor="#000000" width="100%"><tr><td align="center"><img width="150" style="margin: 20px 0" src="https://app.gran.ag/wp-content/themes/gran_sistema/img/logo.png" alt="Hand"></td></tr></table>';
	    $html .= '<br />';
	    $html .= '<p><b>Cadastro de colaborador REGIME PJ</b></p> <br />';
	    $html .= '<b>Nome Fantasia*</b>: '							. $colaborador_pj_nome_fantasia         . '<br />';
	    $html .= '<b>Tipo</b>: '									. $colaborador_pj_tipo            . '<br />';
	    $html .= '<b>Razão Social*</b>: '							. $colaborador_pj_razao_social                    . '<br />';
	    $html .= '<b>CPF/CNJ</b>: '									. $colaborador_pj_cnpj                   . '<br />';
	    $html .= '<b>Email Empresarial</b>: '						. $colaborador_pj_email_empresarial                  . '<br />';
	    $html .= '<b>Telefone Comercial</b>: '						. $colaborador_pj_telefone_comercial             . '<br />';
	    $html .= '<b>Telefone Celular</b>: '						. $colaborador_pj_telefone_celular               . '<br />';
	  	$html .= '<br />';
	    $html .= '<p><b>RESPONSÁVEL LEGAL</b> <br /><p>';
	    $html .= '<b>Nome Completo</b>: '							. $responsavel_legal_nome_completo         . '<br />';
	    $html .= '<b>CPF</b>: '										. $responsavel_legal_cpf                 . '<br />';
	    $html .= '<b>Data de Nascimento</b>: '						. $responsavel_legal_data_nascimento          . '<br />';
	    $html .= '<b>Telefone Celular Pessoal</b>: '				. $responsavel_legal_celular                . '<br />';
	    $html .= '<b>Email pessoal</b>: '							. $responsavel_legal_email              . '<br />';

	    // ENDEREÇO
	    $html .= '<p><b>ENDEREÇO</b></p>';
	    $html .= '<b>CEP:</b>: '               . $colaborador_pj_cep                   . '<br />';
	    $html .= '<b>Endereço:</b>: '          . $colaborador_pj_endereco              . '<br />';
	    $html .= '<b>Número</b>: '             . $colaborador_pj_numero                . '<br />';
	    $html .= '<b>Complemento</b>: '        . $colaborador_pj_complemento           . '<br />';
	    $html .= '<b>Bairro</b>: '             . $colaborador_pj_bairro                . '<br />';
	    $html .= '<b>Cidade</b>: '             . $colaborador_pj_cidade                . '<br />';
	    $html .= '<b>Estado</b>: '             . $colaborador_pj_estado                . '<br />';
	    $html .= '<br />';
	    $html .= '<table align="center" bgcolor="#000000" width="100%"><tr><td align="center"><div style="margin: 20px 0"></div></td></tr></table>';

	    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
	    $envio = wp_mail('carolinohudson@gmail.com', 'Novo colaborador cadastrado na GRAN!', $html);
	    $envio = wp_mail('hudson@gran.ag', 'Novo colaborador cadastrado na GRAN!', $html);
	    $envio = wp_mail('dev@gran.ag', 'Novo colaborador cadastrado na GRAN!', $html);
	    $envio = wp_mail('willerson@gran.ag', 'Novo colaborador cadastrado na GRAN!', $html);
	    remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

    	add_action('wp_ajax_notificacao_cadastroColaborador', 'notificacao_cadastroColaborador');
		add_action('wp_ajax_nopriv_notificacao_cadastroColaborador', 'notificacao_cadastroColaborador');
	}

	die();


}

add_action('wp_ajax_cadastrar_colaborador_pj', 'cadastrar_colaborador_pj');
add_action('wp_ajax_nopriv_cadastrar_colaborador_pj', 'cadastrar_colaborador_pj');







