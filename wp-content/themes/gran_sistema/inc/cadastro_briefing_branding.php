<?php 

function cadastrar_briefing_branding(){
	
	/*****************************************************
	* DADOS DO CONTATO                                   *
	*****************************************************/
		$briefing_branding_nome = 							$_POST['briefing_branding_nome_cliente'];		
		$briefing_branding_email = 							$_POST['briefing_branding_email_cliente'];		
		$briefing_branding_nome_empresa = 					$_POST['briefing_branding_nome_empresa'];		
		$briefing_branding_website_empresa = 				$_POST['briefing_branding_website'];
		$briefing_branding_telefone_celular = 				$_POST['briefing_branding_celular'];
		$briefing_branding_como_encontrou = 				$_POST['briefing_branding_como_encontrou'];
		$briefing_branding_cidade = 						$_POST['briefing_branding_cidade'];
		$briefing_branding_estado = 						$_POST['briefing_branding_estado'];
		
	/*****************************************************
	* INFORMAÇÕES BÁSICAS                                *
	*****************************************************/
		$briefing_branding_novo_redesenho = 				$_POST['briefing_branding_novo_redesenho'];	
		$briefing_branding_quando_entrega = 				$_POST['briefing_branding_quando_entrega'];	
		$briefing_branding_objetivos = 						$_POST['briefing_branding_objetivos'];	

	/*****************************************************
	* LISTA ENTREGA                                      *
	*****************************************************/
		$briefing_branding_manual_identidade = 				$_POST['briefing_branding_manual_identidade'];	
		$briefing_branding_papelaria = 						$_POST['briefing_branding_papelaria'];
		$briefing_branding_papelaria_informacoes_extras = 	$_POST['briefing_branding_papelaria_informacoes_extras'];

	/*****************************************************
	* PERFIL DA EMPRESA                                  *
	*****************************************************/
		$briefing_branding_sobre_empresa = 					$_POST['briefing_branding_sobre_empresa'];	
		$briefing_branding_duas_palavras = 					$_POST['briefing_branding_duas_palavras'];	
		$briefing_branding_uma_palavra = 					$_POST['briefing_branding_uma_palavra'];	

	/*****************************************************
	* SOBRE ASSINATURA                                   *
	*****************************************************/
		$briefing_branding_assinatura = 					$_POST['briefing_branding_assinatura'];	
		$briefing_branding_slogan = 						$_POST['briefing_branding_slogan'];	

	/*****************************************************
	* SOBRE SEUS CLIENTES                                *
	*****************************************************/
		$briefing_branding_sobre_cliente = 					$_POST['briefing_branding_sobre_cliente'];	
		$briefing_branding_deve_transmitir = 				$_POST['briefing_branding_deve_transmitir'];
		$briefing_branding_nao_deve_transmitir = 			$_POST['briefing_branding_nao_deve_transmitir'];

	/****************************************************
	* ARRAY PARA CADASTRAR CUSTOM POST TYPE
	*****************************************************/
		$cadastrar_briefing_branding  = array(
			'post_title'   => $briefing_branding_nome_empresa,
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'	   => 'briefing',
		);	
		$cliente_ID = wp_insert_post($cadastrar_briefing_branding);

	/****************************************************
	* INSERT METABOXES DADOS DO CONTATO
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_briefing_nome', 				$briefing_branding_nome, 							true);
		add_post_meta($cliente_ID, 'Gran_briefing_email', 				$briefing_branding_email, 							true);
		add_post_meta($cliente_ID, 'Gran_briefing_nome_empresa', 		$briefing_branding_nome_empresa, 					true);
		add_post_meta($cliente_ID, 'Gran_briefing_website_empresa', 	$briefing_branding_website_empresa, 				true);
		add_post_meta($cliente_ID, 'Gran_briefing_telefone_celular', 	$briefing_branding_telefone_celular, 				true);
		add_post_meta($cliente_ID, 'Gran_briefing_como_encontrou', 		$briefing_branding_como_encontrou, 					true);
		add_post_meta($cliente_ID, 'Gran_briefing_cidade', 				$briefing_branding_cidade, 							true);
		add_post_meta($cliente_ID, 'Gran_briefing_estado', 				$briefing_branding_estado, 							true);

	/*****************************************************
	* INSERT METABOXES INFORMAÇÕES BÁSICAS               *
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_briefing_novo_redesenho', 		$briefing_branding_novo_redesenho, 					true);
		add_post_meta($cliente_ID, 'Gran_briefing_entrega_projeto', 	$briefing_branding_quando_entrega, 					true);
		add_post_meta($cliente_ID, 'Gran_briefing_objetivos', 			$briefing_branding_objetivos, 						true);

	/*****************************************************
	* INSERT METABOXES LISTA ENTREGA                     *
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_briefing_manual_identidade', 	$briefing_branding_manual_identidade, 				true);
		add_post_meta($cliente_ID, 'Gran_briefing_itens_papelaria', 	$briefing_branding_papelaria, 						true);
		add_post_meta($cliente_ID, 'Gran_briefing_informacoes_extras', 	$briefing_branding_papelaria_informacoes_extras, 	true);

	/*****************************************************
	* INSERT METABOXES PERFIL DA EMPRESA                 *
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_briefing_produtos_servicos', 	$briefing_branding_sobre_empresa, 					true);
		add_post_meta($cliente_ID, 'Gran_briefing_duas_palavras', 		$briefing_branding_duas_palavras, 					true);
		add_post_meta($cliente_ID, 'Gran_briefing_uma_palavra', 		$briefing_branding_uma_palavra, 					true);

	/*****************************************************
	* INSERT METABOXES SOBRE ASSINATURA                  *
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_briefing_texto_assinado', 		$briefing_branding_assinatura, 						true);
		add_post_meta($cliente_ID, 'Gran_briefing_slogan', 				$briefing_branding_slogan, 							true);

	/*****************************************************
	* INSERT METABOXES SOBRE SEUS CLIENTES               *
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_briefing_descricao_clientes', 	$briefing_branding_sobre_cliente, 					true);
		add_post_meta($cliente_ID, 'Gran_briefing_deve_transmitir', 	$briefing_branding_deve_transmitir, 				true);
		add_post_meta($cliente_ID, 'Gran_briefing_nao_deve_transmitir', $briefing_branding_nao_deve_transmitir, 			true);

		// wp_insert_term(
		//     $colaborador_pj_nome_fantasia,  
		//     'categoriacolaborador',
		//     array(
		//         'description' => $logo_detalhes['post_title'],
		//     )
		// );

	if($cliente_ID > 0){
		$cadastroRealizado = true;

		// $user_id = username_exists( $colaborador_primeiro_nome );
		
		// if(!$user_id){
		// 	wp_create_user( $colaborador_primeiro_nome, "123", $colaborador_primeiro_nome . "@gran.ag" );
		// }

	/****************************************************
	* TEMPLATE DE NOTIFICAÇÃO
	*****************************************************/
	    $html .= '<table align="center" bgcolor="#000000" width="100%"><tr><td align="center"><img width="150" style="margin: 20px 0" src="https://app.gran.ag/wp-content/themes/gran_sistema/img/logo.png" alt="Hand"></td></tr></table>';
	    $html .= '<br />';
	    $html .= '<p><b>Cadastro de briefing de branding</b></p> <br />';

	    // DADOS DO CONTATO
	    $html .= '<p><b>Dados do contato</b> <br /><p>';
	    $html .= '<b>Nome do cliente</b>: ' . 													$briefing_branding_nome 							. '<br />';
	    $html .= '<b>Email do cliente</b>: ' . 													$briefing_branding_email 							. '<br />';
	    $html .= '<b>Nome da empresa</b>: ' . 													$briefing_branding_nome_empresa 					. '<br />';
	    $html .= '<b>Website da empresa</b>: ' . 												$briefing_branding_website_empresa 					. '<br />';
	    $html .= '<b>Telefone celular</b>: ' . 													$briefing_branding_telefone_celular 				. '<br />';
	    $html .= '<b>Como encontrou</b>: ' . 													$briefing_branding_como_encontrou 					. '<br />';
	    $html .= '<b>Cidade</b>: ' . 															$briefing_branding_cidade 							. '<br />';
	    $html .= '<b>Estado</b>: ' . 															$briefing_branding_estado 							. '<br />';
	  	$html .= '<br />';

	  	// INFORMAÇÕES BÁSICAS
	    $html .= '<p><b>Informações Básicas</b> <br /><p>';
	    $html .= '<b>Projeto novo ou redesenho</b>: ' . 										$briefing_branding_novo_redesenho 					. '<br />';
	    $html .= '<b>Tempo para entrega</b>: ' . 												$briefing_branding_quando_entrega 					. '<br />';
	    $html .= '<b>Objetivos</b>: ' . 														$briefing_branding_objetivos 						. '<br />';

	    // LISTA DE ENTREGA
	    $html .= '<p><b>Perfil da Empresa</b></p>';
	    $html .= '<b>Precisa de manual de identidade</b>: ' . 									$briefing_branding_manual_identidade 				. '<br />';
	    $html .= '<b>Itens necessários da papelaria</b>: ' . 									$briefing_branding_papelaria 						. '<br />';
	    $html .= '<b>Informações extras sobre a lista de entrega</b>: ' . 						$briefing_branding_papelaria_informacoes_extras 	. '<br />';

	    // PERFIL DA EMPRESA
	    $html .= '<p><b>Lista de Entrega</b></p>';
	    $html .= '<b>Do que se trata/Produtos/Serviços</b>: ' . 								$briefing_branding_sobre_empresa 					. '<br />';
	    $html .= '<b>Descrição em 2 palavras</b>: ' . 											$briefing_branding_duas_palavras 					. '<br />';
	    $html .= '<b>Descrição em 1 palavra</b>: ' . 											$briefing_branding_uma_palavra						. '<br />';

	    // SOBRE ASSINATURA
	    $html .= '<p><b>Sobre Assinatura</b></p>';
	    $html .= '<b>Texto de assinatura</b>: ' . 												$briefing_branding_assinatura 						. '<br />';
	    $html .= '<b>Slogan:</b>: ' . 															$briefing_branding_slogan 							. '<br />';

	    // SOBRE SEUS CLIENTES
	    $html .= '<p><b>Sobre seus Clientes</b></p>';
	    $html .= '<b>Descrição dos clientes(gênero, idade, região, poder aquisitivo)</b>: ' . 	$briefing_branding_sobre_cliente 					. '<br />';
	    $html .= '<b>Mensagem que deve transmitir</b>: ' . 										$briefing_branding_deve_transmitir                	. '<br />';
	    $html .= '<b>Mensagem que NÃO deve transmitir</b>: ' . 									$briefing_branding_nao_deve_transmitir 				. '<br />';
	    $html .= '<br />';
	    $html .= '<table align="center" bgcolor="#000000" width="100%"><tr><td align="center"><div style="margin: 20px 0"></div></td></tr></table>';

	    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
	    $envio = wp_mail('carolinohudson@gmail.com', 'Novo briefing cadastrado na GRAN!', $html);
	    $envio = wp_mail('hudson@gran.ag', 'Novo briefing cadastrado na GRAN!', $html);
	    $envio = wp_mail('dev@gran.ag', 'Novo briefing cadastrado na GRAN!', $html);
	    $envio = wp_mail('willerson@gran.ag', 'Novo briefing cadastrado na GRAN!', $html);
	    $envio = wp_mail('willerson@gran.ag', 'Novo briefing cadastrado na GRAN!', $html);
	    $envio = wp_mail('contato@handgran.com ', 'Novo cliente na Gran!', $html);
	    remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

    	add_action('wp_ajax_notificacao_cadastroColaborador', 'notificacao_cadastroColaborador');
		add_action('wp_ajax_nopriv_notificacao_cadastroColaborador', 'notificacao_cadastroColaborador');
	}

	die();

}

add_action('wp_ajax_cadastrar_briefing_branding', 'cadastrar_briefing_branding');
add_action('wp_ajax_nopriv_cadastrar_briefing_branding', 'cadastrar_briefing_branding');