<?php 
if($_SERVER['REQUEST_METHOD'] == 'POST'){
		
		if(isset($_POST['cadastrar_cliente'])){

			/****************************************************
			* DADOS DO CLIENTE
			*****************************************************/
			$cliente_logo_tmp_name                = $_FILES['cliente_logo']["tmp_name"];		
			$cliente_logo_name                    = $_FILES['cliente_logo']['name'];		
			$cliente_nome_fantasia                = $_POST['cliente_nome_fantasia'];		
			$cliente_tipo                         = $_POST['cliente_tipo'];		
			$cliente_razao_social                 = $_POST['cliente_razao_social'];		
			$cliente_cnpj_cpf                     = $_POST['cliente_cnpj_cpf'];		
			$cliente_inscricao_estadual           = $_POST['cliente_inscricao_estadual'];		
			$cliente_inscricao_municipal          = $_POST['cliente_inscricao_municipal'];		
			$cliente_email_principal              = $_POST['cliente_email_principal'];		
			$cliente_telefone_comercial           = $_POST['cliente_telefone_comercial'];		
			$cliente_telefone_celular             = $_POST['cliente_telefone_celular'];		
			
			/****************************************************
			* DADOS ENDREÇO DO CLIENTE
			*****************************************************/
			$cliente_cep                         = $_POST['cliente_cep'];	
			$cliente_endereco                    = $_POST['cliente_endereco'];	
			$cliente_numero                      = $_POST['cliente_numero'];	
			$cliente_complemento                 = $_POST['cliente_complemento'];	
			$cliente_bairro                      = $_POST['cliente_bairro'];	
			$cliente_cidade                      = $_POST['cliente_cidade'];	
			$cliente_estado                      = $_POST['cliente_estado'];

			/****************************************************
			* DADOS RESPONSÁVEL CLIENTE
			*****************************************************/
			$cliente_responsavel_nome_completo   = $_POST['cliente_responsavel_nome_completo'];	
			$cliente_responsavel_cpf             = $_POST['cliente_responsavel_cpf'];	
			$cliente_responsavel_data_nascimento = $_POST['cliente_responsavel_data_nascimento'];	
			$cliente_responsavel_celular         = $_POST['cliente_responsavel_celular'];	
			$cliente_responsavel_email           = $_POST['cliente_responsavel_email'];

			/****************************************************
			* ARRAY PARA CADASTRAR CLENTES
			*****************************************************/
			$cadastrar_cliente  = array(
		        'post_title'    => $cliente_nome_fantasia,
		        'post_content'  => '',
		        'post_status'   => 'publish',
		        'post_type' 	=> 'cliente'
		    );	
		    $cliente_ID = wp_insert_post($cadastrar_cliente);

			

			/****************************************************
			* INSERT METABOXES  DADOS DO CLIENTE
			*****************************************************/
			
			add_post_meta($cliente_ID, 'Gran_cliente_nome_fantasia',         		$cliente_nome_fantasia,              true);
			add_post_meta($cliente_ID, 'Gran_cliente_tipo',                  		$cliente_tipo,                       true);
			add_post_meta($cliente_ID, 'Gran_cliente_razao_social',          		$cliente_razao_social,               true);
			add_post_meta($cliente_ID, 'Gran_cliente_cnpj_cpf',              		$cliente_cnpj_cpf,                   true);
			add_post_meta($cliente_ID, 'Gran_cliente_inscricao_estadual',    		$cliente_inscricao_estadual,         true);
			add_post_meta($cliente_ID, 'Gran_cliente_inscricao_municipal',   		$cliente_inscricao_municipal,        true);
			add_post_meta($cliente_ID, 'Gran_cliente_email_principal',       		$cliente_email_principal,            true);
			add_post_meta($cliente_ID, 'Gran_cliente_telefone_comercial',    		$cliente_telefone_comercial,         true);
			add_post_meta($cliente_ID, 'Gran_cliente_telefone_celular',      		$cliente_telefone_celular,           true);

			/****************************************************
			* INSERT METABOXES DADOS ENDREÇO DO CLIENTE
			*****************************************************/
			add_post_meta($cliente_ID, 'Gran_cliente_cep',                   		$cliente_cep,                        true);
			add_post_meta($cliente_ID, 'Gran_cliente_endereco',              		$cliente_endereco,                   true);
			add_post_meta($cliente_ID, 'Gran_cliente_numero',                		$cliente_numero,                     true);
			add_post_meta($cliente_ID, 'Gran_cliente_complemento',           		$cliente_complemento,                true);
			add_post_meta($cliente_ID, 'Gran_cliente_bairro',                		$cliente_bairro,                     true);
			add_post_meta($cliente_ID, 'Gran_cliente_cidade',                		$cliente_cidade,                     true);
			add_post_meta($cliente_ID, 'Gran_cliente_estado',                		$cliente_estado,                     true);

			/****************************************************
			* INSERT METABOXES  DADOS RESPONSÁVEL CLIENTE
			*****************************************************/
			add_post_meta($cliente_ID, 'Gran_cliente_responsavel_nome_completo',  $cliente_responsavel_nome_completo,  true);
			add_post_meta($cliente_ID, 'Gran_cliente_responsavel_cpf',            $cliente_responsavel_cpf,            true);
			add_post_meta($cliente_ID, 'Gran_cliente_responsavel_data_nascimento',$cliente_responsavel_data_nascimento,true);
			add_post_meta($cliente_ID, 'Gran_cliente_responsavel_celular',        $cliente_responsavel_celular,        true);
			add_post_meta($cliente_ID, 'Gran_cliente_responsavel_email',          $cliente_responsavel_email,          true);
			
			/****************************************************
			* INSERT METABOXES LOGO DO CLIENTE
			*****************************************************/
			move_uploaded_file($cliente_logo_tmp_name,WP_CONTENT_DIR .'/uploads/clientes/'.basename($cliente_logo_name));

			$logo_nome  = WP_CONTENT_DIR .'/uploads/clientes/' . basename($cliente_logo_name);
			$logo_tipo  = wp_check_filetype( basename( $logo_nome ),null);
			$logo_tipos = array('png','jpg','jpeg','gif');

			if(in_array($logo_tipo['ext'], $logo_tipos)){

				require_once( ABSPATH . 'wp-admin/includes/image.php' );

				$diretorioUploads    = wp_upload_dir();
				$logo_detalhes       = array(
					'guid'           => $diretorioUploads['url'] . '/' . basename( $logo_nome ), 
					'post_mime_type' => $logo_tipo['type'],
					'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $logo_nome ) ),
					'post_content'   => '',
					'post_status'    => 'inherit'
				);

				$logo_carregado_Id 	    = wp_insert_attachment($logo_detalhes, $logo_nome, $cliente_ID);
				$logo_carregado_meta	= wp_generate_attachment_metadata($logo_carregado_Id,$logo_nome);

				wp_update_attachment_metadata($logo_carregado_Id, $logo_carregado_meta);
				add_post_meta($cliente_ID, 'Gran_cliente_logo',$logo_carregado_Id,true);
			}
			
			wp_insert_term(
			    $cliente_nome_fantasia,  
			    'categoriaacesso',
			    array(
			        'description' => $logo_detalhes['post_title'],
			    )
			);
			
			if($cliente_ID > 0){ $cadastroRealizado = true;}
		}
	}

?>