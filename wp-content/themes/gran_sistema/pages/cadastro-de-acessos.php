
<?php

/**
 * Template Name: Cadastro de acessos
 * Description: Cadastro de acessos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */

// DEFINE A TAXONOMIA
$taxonomia = 'categoriaacesso';

// LISTA AS CATEGORIAS
$categoriasacesso = get_terms( $taxonomia, array(
	'orderby'    => 'count',
	'hide_empty' => 0,
	'parent'	 => 0
));
require get_template_directory() . '/inc/cadastro_acesso.php';
get_header(); ?>

<?php if (is_user_logged_in()):?>
<div class="pg pg-cliente">
	<div class="containerFull">

		<!-- CLIENTES -->
		<form  method="post"  enctype="multipart/form-data" id="formulario">
			<div class="row">
				<div class="col-sm-4">
					<figure>
						<!-- <img src="<?php //echo get_template_directory_uri(); ?>/img/logoStardente.png"> -->
					</figure>
				</div>
				<div class="col-sm-8">
					<nav>
						<div class="row">
							<div class="col-sm-7">
								<span>Dados Cadastrais <strong>Acessos</strong></span>
							</div>
							<div class="col-sm-5">
								<div class="formSerarch">
									<label>Selecione o cliente*</label>
									<select  name="acesso_tipo" id="acesso_tipo">
										<?php foreach($categoriasacesso as $categoriasacesso): ?>
								  		<option value="<?php echo $categoriasacesso->term_id?>"><?php echo $categoriasacesso->name;?></option> 
									  	<?php endforeach; ?>
									</select>
									<input type="hidden" name="nome_cliente" value="<?php echo $categoriasacesso->name;?>">
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>

			<!-- TÍTULOS -->
			<section class="hand-title hand-template-desktop">
				
				<div class="row">
					<div class="col-md-4">
						<div class="input_edit input_edit1">
							<strong  class="hand-template-desktop">Canal</strong>
						</div>
						<div class="input_edit input_edit2">
							<strong  class="hand-template-desktop">Login</strong>
						</div>
					</div>
					<div class="col-md-4">
						<div class="input_edit input_edit3">
							<strong  class="hand-template-desktop">Senha</strong>
						</div>
						<div class="input_edit input_edit4">
							<strong  class="hand-template-desktop">URL de acesso</strong>
						</div>
					</div>
					<div class="col-md-4">
						<div class="input_edit input_edit5 text-center">
							<strong  class="hand-template-desktop">OBS</strong>
						</div>
					</div>
				</div>
				
			</section>

			<section>
				<input type="hidden" name="cadastrar_acesso" value="1" class="btnSubmit">
				<input type="submit" id="cadastrar" value="Cadastrar">
				
				<div class="row">
					<div class="col-md-4">
						<strong class="hand-template-mobile">Canal</strong>
						<div class="input_edit input_edit1">
							<input type="text" name="acesso_nome" id="acesso_nome" placeholder="Twitter">
						</div>
						<strong class="hand-template-mobile">Login</strong>
						<div class="input_edit input_edit2">
							<input type="text" name="acesso_login" id="acesso_login" placeholder="agenciahandgran@gmail.com">
						</div>
					</div>
					<div class="col-md-4">
						<strong class="hand-template-mobile">Senha</strong>
						<div class="input_edit input_edit3">
							<input type="text" name="acesso_Senha" id="acesso_Senha" placeholder="*********">
						</div>
						<strong class="hand-template-mobile">URL de acesso</strong>
						<div class="input_edit input_edit4">
							<input type="text" name="acesso_url" id="acesso_url" placeholder="https://twitter.com">
						</div>
					</div>
					<div class="col-md-4">
						<strong class="hand-template-mobile">OBS</strong>
						<div class="input_edit input_edit5">
							<input type="text" name="acesso_obs" id="acesso_obs" placeholder="Social Basic">
						</div>
					</div>
				</div>
				
			</section>
		</form>


	</div>
</div>
<?php endif; ?>
<?php get_footer();