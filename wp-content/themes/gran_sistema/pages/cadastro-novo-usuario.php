<?php

/**
 * Template Name: Cadastro novo usuário
 * Description: Cadastro novo usuário
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */

// require get_template_directory() . '/inc/cadastro_cliente.php';
get_header(); ?>
<div class="pg pg-cadastro-clientes">
	<div class="containerLarge">
		
		<form  method="post"  enctype="multipart/form-data" id="formulario">
			
			<section class="sectionForm">
				<h2 class="hand-title-page">Cadastro de colaborador REGIME PJ</h2>

				<div class="row">
					<div class="col-sm-8">
						<div class="form_box">
							<strong class="hand-title-form">Dados do Empresa MEI</strong>
							<hr>
							<div class="row">
								<div class="col-sm-4">
									<div class="input_type">
										<label>Nome Fantasia*</label>
										<input type="text" required="required" placeholder="Stardente" name="colaborador_pj_nome_fantasia" id="colaborador_pj_nome_fantasia">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input_type">
										<label>Tipo</label>
										<select disabled name="colaborador_pj_tipo" id="colaborador_pj_tipo">
										  <option value="Jurídica">Jurídica</option> 
										  <!-- <option value="Física">Física</option>  -->
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input_type">
										<label>Razão Social*</label>
										<input type="text" required="required" placeholder="Clinica Odontológica Stardente LTDA" name="colaborador_pj_razao_social" id="colaborador_pj_razao_social">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4">
									<div class="input_type">
										<label>CNPJ</label>
										<input type="text" required="required" maxlength="18" placeholder="22.777.663/0001-50" name="colaborador_pj_cnpj_cpf" id="colaborador_pj_cnpj_cpf">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input_type">
										<label>Upload CNH</label>
										<div class="input_type">
											<small class="btnSubmit"><input type="file" required="required" value="Escolher arquivo" name="colaborador_pj_cnh" id="colaborador_pj_cnh"> Escolher arquivo</small>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input_type">
										<p class="nameArquive">NOME_Certidão.PDF</p>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="input_type">
										<label>Email Empresarial</label>
										<input type="text" placeholder="contato@stardente.com.br" name="colaborador_pj_email_empresarial" id="colaborador_pj_email_empresarial">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Telefone Comercial</label>
										<input type="text" placeholder="(41) 31212435" maxlength="14" name="colaborador_pj_telefone_comercial" id="colaborador_pj_telefone_comercial">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Telefone Celular</label>
										<input type="text" placeholder="(41) 9.99413280" maxlength="15" name="colaborador_pj_telefone_celular" id="colaborador_pj_telefone_celular">
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="col-sm-4">
						<strong class="hand-title-form">Responsável legal</strong>
						<hr>	
						<div class="row">
							<div class="col-sm-6">
								<div class="input_type">
									<label>Nome Completo</label>
									<input type="text" placeholder="Felipe Imbeloni" name="colaborador_pj_responsavel_nome_completo" id="colaborador_pj_responsavel_nome_completo">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="input_type">
									<label>CPF</label>
									<input type="text" placeholder="108.266.049-35" maxlength="14"  name="colaborador_pj_responsavel_cpf" id="colaborador_pj_responsavel_cpf">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="input_type">
									<label>Data de Nascimento</label>
									<input type="date" placeholder="25/07/1988" name="colaborador_pj_responsavel_data_nascimento" id="colaborador_pj_responsavel_data_nascimento">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="input_type">
									<label>Telefone Celular Pessoal</label>
									<input type="text" placeholder="(41) 9.9941-3280" name="colaborador_pj_responsavel_celular" id="colaborador_pj_responsavel_celular">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="input_type">
									<label>Email pessoal</label>
									<input type="text" placeholder="felipe@stardente.com.br" name="colaborador_pj_responsavel_email" id="colaborador_pj_responsavel_email">
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>

			<hr>

			<section class="sectionForm">

				<div class="row">
					<div class="col-sm-12">
						<div class="form_box">
							<strong class="hand-title-form">Endereço da Empresa</strong>
							
							<div class="row">
								<div class="col-sm-3">
									<div class="input_type">
										<label>CEP</label>
										<input type="text" placeholder="81.550.340" name="colaborador_pj_cep" id="colaborador_pj_cep">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input_type">
										<label>Endereço</label>
										<input type="text" placeholder="Rua Marechal Deodoro" name="colaborador_pj_endereco" id="colaborador_pj_endereco">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Número</label>
										<input type="text" placeholder="807" name="colaborador_pj_numero" id="colaborador_pj_numero">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-3">
									<div class="input_type">
										<label>Complemento</label>
										<input type="text" placeholder="Sala 890" name="colaborador_pj_complemento" id="colaborador_pj_complemento">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Bairro</label>
										<input type="text" placeholder="Centro" name="colaborador_pj_bairro" id="colaborador_pj_bairro">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Cidade</label>
										<input type="text" placeholder="Curitiba" name="colaborador_pj_cidade" id="colaborador_pj_cidade">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Estado</label>
										<input type="text" placeholder="Paraná" name="colaborador_pj_estado" id="colaborador_pj_estado">
									</div>
								</div>
							</div>

						</div>
					</div>
					<!-- <div class="col-sm-4">
						<strong class="hand-title-form">Arquivos</strong>
						<p>Logo (Se possível em formato PDF/EPS ou PNG)</p>
						<div class="row">
							<div class="col-sm-6">
								<div class="input_type">
									<small class="btnSubmit"><input type="file" value="Escolher arquivo" name="colaborador_pj_logo" id="colaborador_pj_logo"> Escolher arquivo</small>
									
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="input_type">
									<p class="nameArquive">NOME_Certidão.PDF</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="input_type">
									<figure>
										<img src="">
										<figcaption>Exibir Logo</figcaption>	
									</figure>
								</div>
							</div>
						</div>

					</div> -->
				</div>
				<span class="error-message">Campo obrigatório CNH ainda sem arquivo adicionado</span>
			</section>

			<div class="input_type_general">
				<input type="hidden" name="cadastrar_cliente" value="1" class="btnSubmit">
				<input type="submit" id="cadastrar" value="Enviar Dados Cadastrais" class="hidden">
				<input type="" id="btnCadastrar" value="Enviar Dados Cadastrais" name="">
			</div>
		<form>

	</div>
</div>

<?php get_footer();