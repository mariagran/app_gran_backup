<?php

/**
 * Template Name: Home
 * Description: Home
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */
$clientes  = new WP_Query( array( 'post_type' => 'cliente', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );

get_header(); ?>

<?php if (is_user_logged_in()):?>
<div class="pg pg-home">
	<section>
		<div class="containerFull">
			<h2>Clientes</h2>
			<ul>
				<?php while ( $clientes->have_posts() ) : $clientes->the_post();?>
				<li>
					<a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>">
						<figure>
							<img src="<?php echo rwmb_meta('Gran_cliente_logo')['full_url']; ?>">
							<figcaption><?php echo get_the_title(); ?></figcaption>
						</figure>
					</a>
				</li>
				<?php endwhile; ?>
			</ul>
		</div>
	</section>
</div>
<?php endif; ?>
<?php get_footer();