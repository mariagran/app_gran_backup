<?php

/**
 * Template Name: Cadastro de clientes
 * Description: Cadastro de clientes
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */

require get_template_directory() . '/inc/cadastro_cliente.php';
get_header(); ?>
<div class="pg pg-cadastro-clientes">
	<div class="containerLarge">
		
		<form  method="post"  enctype="multipart/form-data" id="formulario">
			
			<section class="sectionForm">

				<span class="cnpj-error-message">CNPJ inválido</span>
				<h2 class="hand-title-page">Cadastro de Cliente</h2>

				<div class="row">
					<div class="col-sm-8">
						<div class="form_box">
							<strong class="hand-title-form">Dados do cliente</strong>
							<hr>
							<div class="row">
								<div class="col-sm-4">
									<div class="input_type">
										<label>Nome Fantasia*</label>
										<input type="text" required="required" placeholder="Nome Fantasia" name="cliente_nome_fantasia" id="cliente_nome_fantasia" class="campo_input_nome_fantasia_validacao">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input_type">
										<label>Tipo*</label>
										<select  name="cliente_tipo" id="cliente_tipo">
										  <option value="Jurídica">Jurídica</option> 
										  <option value="Física">Física</option> 
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input_type">
										<label>Razão Social*</label>
										<input type="text" required="required" placeholder="Razão Social" name="cliente_razao_social" id="cliente_razao_social">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4">
									<div class="input_type">
										<label>CNPJ/CPF*</label>
										<input type="text" required="required" maxlength="18" placeholder="00.000.000/0000-00" name="cliente_cnpj_cpf" id="cliente_cnpj_cpf" class="campo_input_cnpj_validacao">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input_type">
										<label>Inscrição Estadual</label>
										<input type="text" required="required" placeholder="Inscrição Estadual" name="cliente_inscricao_estadual" id="cliente_inscricao_estadual">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input_type">
										<label>Inscrição Municipal</label>
										<input type="text" placeholder="Inscrição Municipal" name="cliente_inscricao_municipal" id="cliente_inscricao_municipal">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="input_type">
										<label>Email Principal</label>
										<input type="text" placeholder="contato@cliente.com.br" name="cliente_email_principal" id="cliente_email_principal" class="campo_input_email_validacao">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Telefone Comercial</label>
										<input type="text" placeholder="(00) 0000-0000" maxlength="14" name="cliente_telefone_comercial" id="cliente_telefone_comercial" class="campo_input_telefone_validacao">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Telefone Celular</label>
										<input type="text" placeholder="(00) 0.00000000" maxlength="15" name="cliente_telefone_celular" id="cliente_telefone_celular">
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="col-sm-4">
						<strong class="hand-title-form">Responsável legal</strong>
						<hr>	
						<div class="row">
							<div class="col-sm-6">
								<div class="input_type">
									<label>Nome Completo</label>
									<input type="text" placeholder="Responsável legal" name="cliente_responsavel_nome_completo" id="cliente_responsavel_nome_completo">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="input_type">
									<label>CPF</label>
									<input type="text" placeholder="000.000.000-00" maxlength="14"  name="cliente_responsavel_cpf" id="cliente_responsavel_cpf">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="input_type">
									<label>Data de Nascimento</label>
									<input type="date" placeholder="25/07/1988" name="cliente_responsavel_data_nascimento" id="cliente_responsavel_data_nascimento">
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="input_type">
									<label>Celular</label>
									<input type="text" placeholder="(00) 0.0000-0000" name="cliente_responsavel_celular" id="cliente_responsavel_celular">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="input_type">
									<label>Email</label>
									<input type="text" placeholder="exemplo@gmail.com" name="cliente_responsavel_email" id="cliente_responsavel_email">
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>

			<hr>

			<section class="sectionForm">

				<div class="row">
					<div class="col-sm-8">
						<div class="form_box">
							<strong class="hand-title-form">Endereço</strong>
							
							<div class="row">
								<div class="col-sm-3">
									<div class="input_type">
										<label>CEP</label>
										<input type="text" placeholder="00.0000.000" name="cliente_cep" id="cliente_cep" class="campo_input_cep_validacao">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input_type">
										<label>Endereço</label>
										<input type="text" placeholder="Rua Marechal Deodoro" name="cliente_endereco" id="cliente_endereco" class="campo_input_endereco_validacao">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Número</label>
										<input type="text" placeholder="000" name="cliente_numero" id="cliente_numero" class="campo_input_numero_validacao">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-3">
									<div class="input_type">
										<label>Complemento</label>
										<input type="text" placeholder="Sala 000" name="cliente_complemento" id="cliente_complemento" class="campo_input_complemento_validacao">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Bairro</label>
										<input type="text" placeholder="Centro" name="cliente_bairro" id="cliente_bairro" class="campo_input_bairro_validacao">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Cidade</label>
										<input type="text" placeholder="Curitiba" name="cliente_cidade" id="cliente_cidade" class="campo_input_cidade_validacao">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input_type">
										<label>Estado</label>
										<input type="text" placeholder="Paraná" name="cliente_estado" id="cliente_estado" class="campo_input_estado_validacao">
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="col-sm-4">
						<strong class="hand-title-form">Arquivos</strong>
						<p>Logo (Se possível em formato PDF/EPS ou PNG)</p>
						<div class="row">
							<div class="col-sm-6">
								<div class="input_type">
									<small class="btnSubmit"><input type="file" value="Escolher arquivo" name="cliente_logo" id="cliente_logo"> Escolher arquivo</small>
									
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="input_type">
									<p class="nameArquive">NOME_Certidão.PDF</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="input_type">
									<figure>
										<img src="">
										<figcaption>Exibir Logo</figcaption>	
									</figure>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>

			<div class="input_type_general">
				<input type="hidden" name="cadastrar_cliente" value="1" class="btnSubmit">
				<input type="submit" id="cadastrar" value="Cadastrar" class="hidden">
				<input type="" id="btnCadastrar" value="Cadastrar" name="">
			</div>
		<form>

	</div>
</div>

<?php get_footer();