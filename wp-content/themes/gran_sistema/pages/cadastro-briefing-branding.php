<?php

/**
 * Template Name: Cadastro de briefing de branding
 * Description: Cadastro de briefing de branding
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */

// require get_template_directory() . '/inc/cadastro_cliente.php';

get_header(); ?>

<div class="pg pg-cadastro-briefing-branding">
	<div class="containerLarge">
		<section class="sectionForm">

			<h2 class="hand-title-page">Briefing - Identidade Visual</h2>

			<div class="form_box" id="formulario">
				<span class="error-message checkbox">Selecione ao menos umas das opções nas seções "Informações Básicas" e "Lista de Entrega".</span>
				<span class="error-message input">Preencha os campos obrigatórios.</span>
				<span class="success-message">Cadastro realizado com sucesso!</span>

				<div class="containerSmall">

					<div class="detalhes-contato">
						<strong class="hand-title-form">Detalhes do Contato</strong>
						<div class="row">
							<div class="col-sm-5">
								<div class="input_type">
									<label>Seu nome</label>
									<input type="text" required="required" placeholder="Seu nome" name="briefing_branding_nome_cliente" id="briefing_branding_nome_cliente" class="">
								</div>
							</div>
							<div class="col-sm-7">
								<div class="input_type">
									<label>Seu email</label>
									<input type="text" required="required" placeholder="exemplo@gmail.com" name="briefing_branding_email_cliente" id="briefing_branding_email_cliente" class="">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="input_type">
									<label>Nome da empresa</label>
									<input type="text" required="required" placeholder="Nome da empresa" name="briefing_branding_nome_empresa" id="briefing_branding_nome_empresa" class="">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-9">
								<div class="input_type">
									<label>Website da empresa</label>
									<input type="text" placeholder="www.seusite.com.br" name="briefing_branding_website" id="briefing_branding_website" class="">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input_type">
									<label>Telefone celular</label>
									<input type="text" placeholder="(00) 0.0000-0000" name="briefing_branding_celular" id="briefing_branding_celular" class="">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-7">
								<div class="input_type">
									<label>Como nos encontrou?</label>
									<input type="text" placeholder="Facebook" name="briefing_branding_como_encontrou" id="briefing_branding_como_encontrou" class="">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input_type">
									<label>Cidade</label>
									<input type="text" placeholder="Curitiba" name="briefing_branding_cidade" id="briefing_branding_cidade" class="">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="input_type">
									<label>Estado</label>
									<input type="text" placeholder="Paraná" name="briefing_branding_estado" id="briefing_branding_estado" class="">
								</div>
							</div>
						</div>
					</div>

					<div class="informacoes-basicas">

						<div class="row">
							<div class="col-sm-12">
								<strong class="hand-title-form">Informações Básicas</strong>
								<strong class="hand-title-form smaller-title">Este é um projeto novo ou redesenho?</strong>
								<div class="input_type">
									<label class="label_radio" for="briefing_branding_novo">
										<input type="radio" name="briefing_branding_novo_redesenho" id="briefing_branding_novo" value="Projeto Novo">
										<span class="input_radio_label">Projeto Novo</span>
									</label>
								</div>
								<div class="input_type">
									<label class="label_radio" for="briefing_branding_redesenho">
										<input type="radio" name="briefing_branding_novo_redesenho" id="briefing_branding_redesenho" value="Redesenho">
										<span class="input_radio_label">Redesenho</span>
									</label>
								</div>
								<strong class="hand-title-form smaller-title">Quando precisa do projeto pronto?</strong>
								<div class="row">
									<div class="col-sm-3">
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_1_mes">
												<input type="radio" name="briefing_branding_quando_entrega" id="briefing_branding_1_mes" value="Em 1 mês">
												<span class="input_radio_label">Em 1 mês</span>
											</label>
										</div>
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_2_meses">
												<input type="radio" name="briefing_branding_quando_entrega" id="briefing_branding_2_meses" value="Em 2 meses">
												<span class="input_radio_label">Em 2 meses</span>
											</label>
										</div>
									</div>
									<div class="col-sm-9">
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_indefinido">
												<input type="radio" name="briefing_branding_quando_entrega" id="briefing_branding_indefinido" value="Indefinido">
												<span class="input_radio_label">Indefinido</span>
											</label>
										</div>
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_urgente">
												<input type="radio" name="briefing_branding_quando_entrega" id="briefing_branding_urgente" value="Urgente">
												<span class="input_radio_label">Urgente*</span>
											</label>
										</div>
									</div>
								</div>
								<span class="span_observacao">*Se você tem ungência é possível que eu consiga ajudá-lo a realizar o projeto em menos de um mês. <br>Neste caso, uma taxa de urgência será aplicada sobre o valor total do projeto.</span>

								<p>Para o que você precisa deste projeto? Quais são seus objetivos?</p>
								<div class="input_type">
									<textarea name="briefing_branding_objetivos" id="briefing_branding_objetivos"></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="lista-entrega">
						<div class="row">
							<div class="col-sm-12">

								<strong class="hand-title-form">Lista de Entrega</strong>

								<p class="descricao">Selecione abaixo os itens que pretende realizar o design neste momento. Em alguns aspectos é possível que eu não possa ajudá-lo, portando, caso sinta dalta de algum ponto entre em contato para discurtirmos as opções.</p>

								<p>Precisa de um manual de identidade de marca?</p>
								<div class="row">
									<div class="col-sm-4">
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_manual_identidade_sim">
												<input type="radio" name="briefing_branding_manual_identidade" id="briefing_branding_manual_identidade_sim" value="Sim">
												<span class="input_radio_label">Sim</span>
											</label>
										</div>
									</div>
									<div class="col-sm-8">
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_manual_identidade_nao">
												<input type="radio" name="briefing_branding_manual_identidade" id="briefing_branding_manual_identidade_nao" value="Não">
												<span class="input_radio_label">Não</span>
											</label>
										</div>
									</div>
								</div>
								<span class="span_observacao"><strong>Dica:</strong> Não sabe o que é um manual de identidade de marca? <a href="#">Clique Aqui</a></span>

								<p>Selecione os itens de papelaria necessários</p>
								<div class="row">
									<div class="col-sm-4">
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_papelaria_cartao">
												<input type="checkbox" name="briefing_branding_papelaria" id="briefing_branding_papelaria_cartao" value="Cartão de visitas">
												<span class="input_radio_label">Cartão de visitas</span>
											</label>
										</div>
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_papelaria_cracha">
												<input type="checkbox" name="briefing_branding_papelaria" id="briefing_branding_papelaria_cracha" value="Crachá">
												<span class="input_radio_label">Crachá</span>
											</label>
										</div>
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_papelaria_envelope">
												<input type="checkbox" name="briefing_branding_papelaria" id="briefing_branding_papelaria_envelope" value="Envelope">
												<span class="input_radio_label">Envelope</span>
											</label>
										</div>
									</div>
									<div class="col-sm-8">
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_papelaria_pasta_a4">
												<input type="checkbox" name="briefing_branding_papelaria" id="briefing_branding_papelaria_pasta_a4" value="Pasta A4">
												<span class="input_radio_label">Pasta A4</span>
											</label>
										</div>
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_papelaria_timbrado">
												<input type="checkbox" name="briefing_branding_papelaria" id="briefing_branding_papelaria_timbrado" value="Timbrado DOC">
												<span class="input_radio_label">Timbrado DOC</span>
											</label>
										</div>
										<div class="input_type">
											<label class="label_radio" for="briefing_branding_papelaria_outros">
												<input type="checkbox" name="briefing_branding_papelaria" id="briefing_branding_papelaria_outros" value="Outros">
												<span class="input_radio_label">Outros</span>
											</label>
										</div>
									</div>
								</div>

								<p>Informações extras sobre a lista de entrega</p>
								<span class="span_observacao span_observacao_not_italic">Utilize este campo para me dizer se faltou algum item ou forneça detalhes sobre os itens selecionados.</span>
								<div class="input_type">
									<textarea name="briefing_branding_papelaria_informacoes_extras" id="briefing_branding_papelaria_informacoes_extras"></textarea>
								</div>
							</div>
						</div>
					</div>

					<div class="perfil-empresa">
						<div class="row">
							<div class="col-sm-12">

								<strong class="hand-title-form">Perfil da Empresa</strong>

								<p class="descricao">Me fale sobre sua empresa</p>
								<span class="span_observacao span_observacao_not_italic">Do que se trata? Quais são os produtos/serviços oferecidos? Há quanto tempo existe? Qual sua missão, visão e valores?</span>
								<div class="input_type">
									<textarea name="briefing_branding_sobre_empresa" id="briefing_branding_sobre_empresa"></textarea>
								</div>

								<p>Descreva seu negócio em 2 palavras</p>
								<div class="input_type">
									<input type="text" name="briefing_branding_duas_palavras" id="briefing_branding_duas_palavras">
								</div>

								<p>Descreva seu negócio em 1 palavra</p>
								<div class="input_type">
									<input type="text" name="briefing_branding_uma_palavra" id="briefing_branding_uma_palavra">
								</div>
							</div>
						</div>
					</div>

					<div class="sobre-assinatura">
						<div class="row">
							<div class="col-sm-12">
								<strong class="hand-title-form">Sobre Assinatura</strong>

								<p>Qual será o texto assinado na marca?</p>
								<div class="input_type">
									<input type="text" name="briefing_branding_assinatura" id="briefing_branding_assinatura">
								</div>
								<span class="span_observacao"><strong>Exemplo:</strong> McDonalds</span>

								<p>Possui slogan? Tagline? Qual o texto?</p>
								<div class="input_type">
									<input type="text" name="briefing_branding_slogan" id="briefing_branding_slogan">
								</div>
								<span class="span_observacao"><strong>Exemplo:</strong> Amo muito tudo isso</span>
							</div>
						</div>
					</div>

					<div class="sobre-cliente">
						<div class="row">
							<div class="col-sm-12">
								<strong class="hand-title-form">Sobre seus Clientes</strong>

								<p class="descricao">Descreva o máximo que puder sobre seus clientes. Gênero, idade, região, poder aquisito, etc.</p>
								<div class="input_type">
									<textarea name="briefing_branding_sobre_cliente" id="briefing_branding_sobre_cliente"></textarea>
								</div>

								<p>Que mensagens sua marca deve transmitir para seus cliente?</p>
								<div class="input_type">
									<input type="text" name="briefing_branding_deve_transmitir" id="briefing_branding_deve_transmitir">
								</div>
								<span class="span_observacao"><strong>Exemplo:</strong> Somos populares, porém informativos e intelectuais.</span>

								<p>Que mensagens sua marca NÃO deve transmitir para seus cliente?</p>
								<div class="input_type">
									<input type="text" name="briefing_branding_nao_deve_transmitir" id="briefing_branding_nao_deve_transmitir">
								</div>
								<span class="span_observacao"><strong>Exemplo:</strong> Somos populares, porém informativos e intelectuais.</span>
							</div>
						</div>
					</div>

				</div>
				<span class="success-message">Cadastro realizado com sucesso! Você será redirecionado para a página inicial.</span>
			</div>
		</section>

		<div class="input_type_general">
			<input type="hidden" name="cadastrar_briefing" value="1" class="btnSubmit">
			<input type="submit" id="cadastrarBriefing" value="Cadastrar" class="hidden">
			<input type="" id="btnCadastrarBriefing" value="Cadastrar" name="">
		</div>
	</div>
</div>

<?php get_footer();