$(function(){


	/*************************************************
	 			FUNÇÃO PLACEHOLDER FORMULÁRIO
	*************************************************/
	//ATIVANDO O FOCUS E RECUPERANDO PLACEHOLDER
	$( ".pg-cadastro-briefing-branding .input_type input[type='text']" ).focusin(function(e) {
	  	let placeholder = $(this).attr('placeholder'); 
	  	
	  	//ATIVA FUNÇÃO VOLTAR PLACEHOLDER
	  	placeholderValor(placeholder);
	 
	 	//RESENTANDO PLACEHOLDER
	 	$(this).attr('placeholder', '');
	});

	// FUNÇÃO FOCUS AOUT PARA ATIVAR PLACEHOLDER
	function placeholderValor(placeholder){
		$( ".pg-cadastro-briefing-branding .input_type input[type='text']" ).focusout(function(e){
		   $(this).attr('placeholder', placeholder);
		});
	}

	/*************************************************
	 	ADCIONAR MASCARAS NOS CAMPOS DE TEXTO 
	*************************************************/
	maskForm();
	function maskForm(){

		const $briefing_telefone_celular = $("#briefing_branding_celular");
		$briefing_telefone_celular.mask('(00)0.0000-0000', {reverse: false});

	}

	/*************************************************
	 			FUNÇÃO DE CADASTRO 
	*************************************************/
	var btnCadastrar = document.querySelector(".pg-cadastro-briefing-branding #btnCadastrarBriefing");
	
	//FUNÇÃO CLIQUE
	btnCadastrar.addEventListener("click", function(){ 
		var inputList = document.querySelectorAll(".pg-cadastro-briefing-branding input[required='required']");
		var radioList = document.querySelectorAll(".pg-cadastro-briefing-branding input[type='radio']");
		var checkboxList = document.querySelectorAll(".pg-cadastro-briefing-branding input[type='checkbox']");
		console.log(radioList);
		validacaoCampos(inputList, radioList, checkboxList);
	});

	let elementType = '';
	function validacaoCampos(inputList, radioList, checkboxList){

		var inputIsValid = false;
		let inputRadioIsValid = false;
		let checkboxIsValid = false;

		// VALIDANDO INPUTS TIPO TEXTO
		for(let i = 0; i < inputList.length; i++){

			const element      = inputList[i];
			const elementValue = inputList[i].value;
			elementType        = inputList[i].getAttribute('type');

			inputIsValid = verificacaoCampos(elementValue,element);
		}

		// VALIDANDO INPUTS TIPO RADIO
		for(let i = 0; i < radioList.length; i++){

			let inputRadioName = radioList[i].getAttribute('name');
			let inputRadio = document.getElementsByName(inputRadioName);

			inputRadioIsValid = validacaoInputsRadio(inputRadio);

		}

		// VALIDANDO CHECKBOX
		for(let i = 0; i < checkboxList.length; i++){

			let checkboxName = checkboxList[i].getAttribute('name');
			let checkbox = document.getElementsByName(checkboxName);

			checkboxIsValid = validacaoCheckbox(checkbox);

		}
		if(inputIsValid && inputRadioIsValid && checkboxIsValid){
			cadastrarBriefingBranding();
		}
	}

	function verificacaoCampos(elementValue,element){
		if (elementValue == "" || elementValue == undefined){
			element.classList.add("campo-required");

			document.querySelector("span.error-message.input").classList.add("error-message-active");
			$('html,body').animate({
				scrollTop: $("#formulario").offset().top
			}, 1000);
			inputIsValid = false;

		}else{
			element.classList.remove("campo-required");
			document.querySelector("span.error-message.input").classList.remove("error-message-active");
			inputIsValid = true;
		}
		return inputIsValid;
	}

	// VALIDAÇÃO DE INPUTS TIPO RADIO
	function validacaoInputsRadio(inputRadio){
		let inputRadioIsValid = false;

		for(let i = 0; i < inputRadio.length; i++){
			if(inputRadio[i].checked){
				document.querySelector("span.error-message.checkbox").classList.remove("error-message-active");
				return true;
			} else{
				document.querySelector("span.error-message.checkbox").classList.add("error-message-active");
				$('html,body').animate({
					scrollTop: $("#formulario").offset().top
				}, 500);
			}
		}
		return false;
	}

	// RECUPERAR VALOR DOS INPUTS RADIO
	function getInputRadioValue(inputName){
		let inputsRadio = document.getElementsByName(inputName);

		for(let i = 0; i < inputsRadio.length; i++){
			if(inputsRadio[i].checked){
				return inputsRadio[i].value;
			}
		}

		return null;
	}

	// VALIDAÇÃO DE CHECKBOX
	function validacaoCheckbox(checkbox){

		for(let i = 0; i < checkbox.length; i++){
			if(checkbox[i].checked){
				document.querySelector("span.error-message.checkbox").classList.remove("error-message-active");
				return true;
			} else{
				document.querySelector("span.error-message.checkbox").classList.add("error-message-active");
				$('html,body').animate({
					scrollTop: $("#formulario").offset().top
				}, 500);
			}
		}
		return false;

	}

	// RECUPERAR VALOR(ES) DO CHECKBOX
	function getInputCheckboxValue(inputName){
		let inputsCheckbox = document.getElementsByName(inputName);
		let inputsCheckboxList = [];
		let inputsCheckboxJoin = '';

		for(let i = 0; i < inputsCheckbox.length; i++){
			if(inputsCheckbox[i].checked){
				inputsCheckboxList.push(inputsCheckbox[i].value);
			}
		}

		inputsCheckboxJoin = inputsCheckboxList.join();

		return inputsCheckboxJoin;
	}

	function cadastrarBriefingBranding(){

		// DADOS DO CONTATO
		var briefing_branding_nome_cliente 					= $('#briefing_branding_nome_cliente').val();
		var briefing_branding_email_cliente 				= $('#briefing_branding_email_cliente').val();
		var briefing_branding_nome_empresa 					= $('#briefing_branding_nome_empresa').val();
		var briefing_branding_website 						= $('#briefing_branding_website').val();
		var briefing_branding_celular 						= $('#briefing_branding_celular').val();
		var briefing_branding_como_encontrou 				= $('#briefing_branding_como_encontrou').val();
		var briefing_branding_cidade 						= $('#briefing_branding_cidade').val();
		var briefing_branding_estado 						= $('#briefing_branding_estado').val();

		// INFORMAÇÕES BÁSICAS
		var briefing_branding_novo_redesenho 				= getInputRadioValue('briefing_branding_novo_redesenho');
		var briefing_branding_quando_entrega 				= getInputRadioValue('briefing_branding_quando_entrega');
		var briefing_branding_objetivos 					= $('#briefing_branding_objetivos').val();

		// LISTA DE ENTREGA
		var briefing_branding_manual_identidade 			= getInputRadioValue('briefing_branding_manual_identidade');
		var briefing_branding_papelaria 					= getInputCheckboxValue('briefing_branding_papelaria');
		var briefing_branding_papelaria_informacoes_extras 	= $('#briefing_branding_papelaria_informacoes_extras').val();

		// PERFIL DA EMPRESA
		var briefing_branding_sobre_empresa 				= $('#briefing_branding_sobre_empresa').val();
		var briefing_branding_duas_palavras 				= $('#briefing_branding_duas_palavras').val();
		var briefing_branding_uma_palavra 					= $('#briefing_branding_uma_palavra').val();

		// SOBRE ASSINATURA
		var briefing_branding_assinatura 					= $('#briefing_branding_assinatura').val();
		var briefing_branding_slogan 						= $('#briefing_branding_slogan').val();

		// SOBRE OS CLIENTES
		var briefing_branding_sobre_cliente					= $('#briefing_branding_sobre_cliente').val();
		var briefing_branding_deve_transmitir 				= $('#briefing_branding_deve_transmitir').val();
		var briefing_branding_nao_deve_transmitir 			= $('#briefing_branding_nao_deve_transmitir').val();

		// if(colaborador_pj_nome_fantasia != '' && colaborador_pj_responsavel_email != ''){

	  	// AJAX
	  	$.ajax({

	  		url: 'https://app.gran.ag/wp-admin/admin-ajax.php',
	  		type: 'POST',
	  		data : {
	  			action: "cadastrar_briefing_branding", 

		      	// DADOS DO CONTATO
		      	briefing_branding_nome_cliente: 					briefing_branding_nome_cliente, 
		      	briefing_branding_email_cliente: 					briefing_branding_email_cliente, 
		      	briefing_branding_nome_empresa: 					briefing_branding_nome_empresa, 
		      	briefing_branding_website: 							briefing_branding_website, 
		      	briefing_branding_celular: 							briefing_branding_celular, 
		      	briefing_branding_como_encontrou: 					briefing_branding_como_encontrou, 
		      	briefing_branding_cidade: 							briefing_branding_cidade, 
		      	briefing_branding_estado: 							briefing_branding_estado, 

		      	// INFORMAÇÕES BÁSICAS
		      	briefing_branding_novo_redesenho: 					briefing_branding_novo_redesenho,
		      	briefing_branding_quando_entrega: 					briefing_branding_quando_entrega,
		      	briefing_branding_objetivos: 						briefing_branding_objetivos,

		      	// LISTA DE ENTREGA
		      	briefing_branding_manual_identidade: 				briefing_branding_manual_identidade, 
		      	briefing_branding_papelaria: 						briefing_branding_papelaria, 
		      	briefing_branding_papelaria_informacoes_extras: 	briefing_branding_papelaria_informacoes_extras, 

		      	// PERFIL DA EMPRESA
		      	briefing_branding_sobre_empresa: 					briefing_branding_sobre_empresa, 
		      	briefing_branding_duas_palavras: 					briefing_branding_duas_palavras, 
		      	briefing_branding_uma_palavra: 						briefing_branding_uma_palavra, 

		      	// SOBRE ASSINATURA
		      	briefing_branding_assinatura: 						briefing_branding_assinatura, 
		      	briefing_branding_slogan: 							briefing_branding_slogan, 

		      	// SOBRE OS CLIENTES
		      	briefing_branding_sobre_cliente: 					briefing_branding_sobre_cliente, 
		      	briefing_branding_deve_transmitir: 					briefing_branding_deve_transmitir, 
		      	briefing_branding_nao_deve_transmitir: 				briefing_branding_nao_deve_transmitir, 
		      },

		      success: function (resp) {

		      	console.log("top");
		      	$('span.success-message').addClass('success-message-active');
		      	setTimeout(function(){
		      		location.href = "https://gran.ag/";
		      	}, 8000);

		      }

		  });

		// }
}
		

});