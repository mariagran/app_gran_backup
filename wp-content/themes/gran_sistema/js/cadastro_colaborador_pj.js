$(function(){


	/*************************************************
	 			FUNÇÃO PLACEHOLDER FORMULÁRIO
	*************************************************/
	//ATIVANDO O FOCUS E RECUPERANDO PLACEHOLDER
	$( ".page-template-cadastro-funcionario-pj  .pg-cadastro-clientes .input_type input[type='text']" ).focusin(function(e) {
	  	let placeholder = $(this).attr('placeholder'); 
	  	
	  	//ATIVA FUNÇÃO VOLTAR PLACEHOLDER
	  	placeholderValor(placeholder);
	 
	 	//RESENTANDO PLACEHOLDER
	 	$(this).attr('placeholder', '');
	});

	// FUNÇÃO FOCUS AOUT PARA ATIVAR PLACEHOLDER
	function placeholderValor(placeholder){
		$( ".page-template-cadastro-funcionario-pj  .pg-cadastro-clientes .input_type input[type='text']" ).focusout(function(e){
		   $(this).attr('placeholder', placeholder);
		});
	}

	/*************************************************
	 			FUNÇÃO BUSCA CEP
	*************************************************/
	$(".page-template-cadastro-funcionario-pj  #colaborador_pj_cep").focusout(function(e){
	  const colaborador_pj_cep = $(this).val();
	  buscaCep(colaborador_pj_cep);
	});

	//REQUEST JASON
	function buscaCep(colaborador_pj_cep) {
		const url = 'https://webmaniabr.com/api/1/cep/'+colaborador_pj_cep+'/?app_key=KJBVOAc5bQD03qO3bELxe8qqQjQDk1gr&app_secret=YbT7ViLMqOujNzoZcnXrkocpw3J8fqHz1AteWoDpVYxWBHJL'
		
		return fetch(url)
			
			.then( response => {

				const response_cep = response.json();
				response_cep.then(function(result_cep) {
					insertEndereco(result_cep);

				})

	      return response.json();

	    })

	    .catch(e => {
	    	insertEnderecoError()
	      	return false
	    })

	}

	//FUNÇÃO DE PREENCHIMENTO DOS CAMPOS
	function insertEndereco(result_cep){
		$(".page-template-cadastro-funcionario-pj  #colaborador_pj_endereco").val(result_cep.endereco)
		$(".page-template-cadastro-funcionario-pj  #colaborador_pj_bairro").val(result_cep.bairro)
		$(".page-template-cadastro-funcionario-pj  #colaborador_pj_cidade").val(result_cep.cidade)
		$(".page-template-cadastro-funcionario-pj  #colaborador_pj_estado").val(result_cep.uf)
		$(".page-template-cadastro-funcionario-pj  #colaborador_pj_cep").val(result_cep.cep)
	}

	function insertEnderecoError(){
		console.log("Cep inválido");
	}


	/*************************************************
	 	ADCIONAR MASCARAS NOS CAMPOS DE TEXTO 
	*************************************************/
	maskForm();
	function maskForm(){

		const $colaborador_pj_cnpj = $(".page-template-cadastro-funcionario-pj #colaborador_pj_cnpj");
		$colaborador_pj_cnpj.mask('00.000.000/0000-00', {reverse: true});

		const $colaborador_pj_telefone_comercial = $("#colaborador_pj_telefone_comercial");
		$colaborador_pj_telefone_comercial.mask('(00) 0000-0000', {reverse: false});

		const $colaborador_pj_telefone_celular = $("#colaborador_pj_telefone_celular");
		$colaborador_pj_telefone_celular.mask('(00)0.00000000', {reverse: false});

		const $colaborador_pj_responsavel_cpf = $("#responsavel_legal_cpf");
		$colaborador_pj_responsavel_cpf.mask('000.000.000-00', {reverse: false});

		const $colaborador_pj_responsavel_celular = $("#responsavel_legal_celular");
		$colaborador_pj_responsavel_celular.mask('(00)0.00000000', {reverse: false});
	}

	/*************************************************
	 			FUNÇÃO DE CADASTRO 
	*************************************************/
	var btnCadastrar = document.querySelector(".page-template-cadastro-funcionario-pj #btnCadastrar");
	
	//FUNÇÃO CLIQUE
	btnCadastrar.addEventListener("click", function(){ 
		var inputList = document.querySelectorAll(".page-template-cadastro-funcionario-pj input[required='required']");
		validacaoCampos(inputList);
	});

	let elementType = '';
	function validacaoCampos(inputList){

		var inputIsValid = false;
		for(let i = 0; i< inputList.length;i++){

			const element      = inputList[i];
			const elementValue = inputList[i].value;
			elementType        = inputList[i].getAttribute('type');

			inputIsValid = verificacaoCampos(elementValue,element);
		}
		if(inputIsValid){
			cadastrarColaboradorPj();
		}
	}

	function verificacaoCampos(elementValue,element){
		if (elementValue == "" || elementValue == undefined){
			element.classList.add("campo-required");
			if(elementType == "file"){
				document.querySelector("span.error-message").classList.add("error-message-active");
			}
			$('html,body').animate({
				scrollTop: $("#formulario").offset().top
			}, 1000);
			inputIsValid = false;
		}else{
			element.classList.remove("campo-required");
			document.querySelector("span.error-message").classList.remove("error-message-active");
			inputIsValid = true;
		}
		return inputIsValid;
	}

	function recuperarPrimeiroNomeColaborador(){
		let nome_completo = $('#responsavel_legal_nome_completo').val();
		let array_nomes = nome_completo.split(" ");
		let primeiro_nome = array_nomes[0];
		primeiro_nome = primeiro_nome.toLowerCase();

		return primeiro_nome;
	}

	function cadastrarColaboradorPj(){

		// PRIMEIRO NOME DO RESPONSÁVEL LEGAL
		var colaborador_primeiro_nome  		  = recuperarPrimeiroNomeColaborador();

		// DADOS EMRPESA MEI
		var colaborador_pj_nome_fantasia      = $('#colaborador_pj_nome_fantasia').val();
		var colaborador_pj_tipo               = $('#colaborador_pj_tipo').val();
		var colaborador_pj_razao_social       = $('#colaborador_pj_razao_social').val();
		var colaborador_pj_cnpj               = $('#colaborador_pj_cnpj').val();
		var colaborador_pj_cnh  			  = $('#colaborador_pj_cnh').val();
		var colaborador_pj_email_empresarial  = $('#colaborador_pj_email_empresarial').val();
		var colaborador_pj_telefone_comercial = $('#colaborador_pj_telefone_comercial').val();
		var colaborador_pj_telefone_celular   = $('#colaborador_pj_telefone_celular').val();

		// DADOS RESPONSÁVEL LEGAL
		var responsavel_legal_nome_completo   = $('#responsavel_legal_nome_completo').val();
		var responsavel_legal_cpf 			  = $('#responsavel_legal_cpf').val();
		var responsavel_legal_data_nascimento = $('#responsavel_legal_data_nascimento').val();
		var responsavel_legal_celular         = $('#responsavel_legal_celular').val();
		var responsavel_legal_email           = $('#responsavel_legal_email').val();

		// ENDEREÇO DA EMPRESA
		var colaborador_pj_cep                = $('#colaborador_pj_cep').val();
		var colaborador_pj_endereco           = $('#colaborador_pj_endereco').val();
		var colaborador_pj_numero             = $('#colaborador_pj_numero').val();
		var colaborador_pj_complemento        = $('#colaborador_pj_complemento').val();
		var colaborador_pj_bairro             = $('#colaborador_pj_bairro').val();
		var colaborador_pj_cidade             = $('#colaborador_pj_cidade').val();
		var colaborador_pj_estado             = $('#colaborador_pj_estado').val();

		// if(colaborador_pj_nome_fantasia != '' && colaborador_pj_responsavel_email != ''){

	  	// AJAX
	  	$.ajax({

	  		url: 'https://app.gran.ag/wp-admin/admin-ajax.php',
	  		type: 'POST',
	  		data : {
	  			action: "cadastrar_colaborador_pj", 

	  			// PRIMEIRO NOME
		      	colaborador_primeiro_nome 			:colaborador_primeiro_nome, 

		      	// DADOS EMRPESA
		      	colaborador_pj_nome_fantasia:      colaborador_pj_nome_fantasia, 
		      	colaborador_pj_tipo:               colaborador_pj_tipo, 
		      	colaborador_pj_razao_social:       colaborador_pj_razao_social, 
		      	colaborador_pj_cnpj:               colaborador_pj_cnpj, 
		      	colaborador_pj_cnh:  			   colaborador_pj_cnh, 
		      	colaborador_pj_email_empresarial:  colaborador_pj_email_empresarial, 
		      	colaborador_pj_telefone_comercial: colaborador_pj_telefone_comercial, 
		      	colaborador_pj_telefone_celular:   colaborador_pj_telefone_celular, 

		      	// DADOS RESPONSÁVEL
		      	responsavel_legal_nome_completo:   responsavel_legal_nome_completo,
		      	responsavel_legal_cpf: 			   responsavel_legal_cpf,
		      	responsavel_legal_data_nascimento: responsavel_legal_data_nascimento,
		      	responsavel_legal_celular:         responsavel_legal_celular,
		      	responsavel_legal_email:           responsavel_legal_email,

		      	// ENDEREÇO
		      	colaborador_pj_cep:         colaborador_pj_cep, 
		      	colaborador_pj_endereco:    colaborador_pj_endereco, 
		      	colaborador_pj_numero:      colaborador_pj_numero, 
		      	colaborador_pj_complemento: colaborador_pj_complemento, 
		      	colaborador_pj_bairro:      colaborador_pj_bairro, 
		      	colaborador_pj_cidade:      colaborador_pj_cidade, 
		      	colaborador_pj_estado:      colaborador_pj_estado, 
		      },

		      success: function (resp) {

		      	console.log("top");
		      	$('span.success-message').addClass('success-message-active');
		      	setTimeout(function(){
		      		location.href = "https://gran.ag/";
		      	}, 8000);

		      }

		  });

		// }
}
		

});