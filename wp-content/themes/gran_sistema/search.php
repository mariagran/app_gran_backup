<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Gran_Sistema
 */

get_header();
?>

	<div class="pg pg-cliente">
	<div class="containerFull">

		<!-- CLIENTES -->
		<div class="row">
			<div class="col-sm-4">
			
			</div>
			<div class="col-sm-8">
				<nav>
					<div class="row">
						<div class="col-sm-7">
							<span><?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Resultado da pesquisa por: %s', 'Gran_Sistema' ), '<strong>' . get_search_query() . '</strong>' );
					?></span>
						</div>
						<div class="col-sm-5">
							<div class="formSerarch">
								<label>Pesquisar*</label>
								<div class="formSerarch">
									<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
										<input type="text" name="s" id="search" placeholder="Buscar">
										<input type="submit" name="">
									</form>
								</div>
							</div>
						</div>
					</div>
				</nav>
			</div>
		</div>

		<!-- TÍTULOS -->
		<section class="hand-title hand-template-desktop">
			
			<div class="row">
				<div class="col-md-4">
					<div class="input_edit input_edit1">
						<strong  class="hand-template-desktop">Canal</strong>
					</div>
					<div class="input_edit input_edit2">
						<strong  class="hand-template-desktop">Login</strong>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input_edit input_edit3">
						<strong  class="hand-template-desktop">Senha</strong>
					</div>
					<div class="input_edit input_edit4">
						<strong  class="hand-template-desktop">URL de acesso</strong>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input_edit input_edit5 text-center">
						<strong  class="hand-template-desktop">OBS</strong>
					</div>
				</div>
			</div>
			
		</section>

		<?php while ( have_posts() ) : the_post(); ?>
		<section>
			<!-- <input type="hidden" name="cadastrar_acesso" value="1" class="btnSubmit">
			<input type="submit" id="cadastrar" value="Cadastrar"> -->
			
			<div class="row">
				<div class="col-md-4">
					<strong class="hand-template-mobile">Canal</strong>
					<div class="input_edit input_edit1">
						<input type="text" name="acesso_nome" value="<?php echo get_the_title() ?>" id="acesso_nome" placeholder="Twitter">
					</div>
					<strong class="hand-template-mobile">Login</strong>
					<div class="input_edit input_edit2">
						<input type="text" name="acesso_login" id="acesso_login" value="<?php echo rwmb_meta('Gran_acesso_login'); ?>" placeholder="agenciahandgran@gmail.com">
					</div>
				</div>
				<div class="col-md-4">
					<strong class="hand-template-mobile">Senha</strong>
					<div class="input_edit input_edit3">
						<input type="text" name="acesso_Senha" id="acesso_Senha" value="<?php echo rwmb_meta('Gran_acesso_Senha'); ?>" placeholder="*********">
					</div>
					<strong class="hand-template-mobile">URL de acesso</strong>
					<div class="input_edit input_edit4">
						<input type="text" name="acesso_url" id="acesso_url" value="<?php echo rwmb_meta('Gran_acesso_url'); ?>" placeholder="https://twitter.com">
					</div>
				</div>
				<div class="col-md-4">
					<strong class="hand-template-mobile">OBS</strong>
					<div class="input_edit input_edit5">
						<input type="text" name="acesso_obs" id="acesso_obs" value="<?php echo rwmb_meta('Gran_acesso_obs'); ?>" placeholder="Social Basic">
					</div>
				</div>
			</div>
			
		</section>
		<?php endwhile; ?>
	


	</div>
</div>

<?php

get_footer();
